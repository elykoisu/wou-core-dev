#include "DISPLAY7SEG.h"
#include <stdint.h>


/*----- DEFINES SECTION -----*/

/*-- CMD --*/
#define CLEAR 0x76
#define DEC_CONTROL 0x77
#define RESET 0x81
#define BRIGHT_CONT 0x7A

/***VARS***/
char display[4];
uint8_t dec_control[2];
uint8_t err;
mraa_uart_context UART;

/**********/
/*-- PROTO --*/
// Issues the RESET command, code found in defines
void DISPLAY7SEG_reset(){
	mraa_uart_write(UART, RESET);
}
// Issues the CLEAR command, code found in defines
void DISPLAY7SEG_clear(){
	mraa_uart_write(UART, CLEAR);
}
// Initializes the 7-SEG with a selected BAUD, BAUD Support found in defines
void DISPLAY7SEG_init(uint8_t RX, uint8_t BAUD){
	UART = mraa_uart_init(RX);
	mraa_uart_set_baudrate(UART, BAUD);
}
// Initializes the 7-SEG with default BAUD of 9600, BAUD Support found in defines
void DISPLAY7SEG_init(uint8_t RX){
	UART = mraa_uart_init(RX);
	mraa_uart_set_baudrate(UART, 9600);
}

// Sets Digit 1
void DISPLAY7SEG_setD1(char d){
	display[0] = d;
	DISPLAY7SEG_setDisplay(display);
}
// Sets Digit 2
void DISPLAY7SEG_setD2(char d){
	display[1] = d;
	DISPLAY7SEG_setDisplay(display);
}
// Sets Digit 3
void DISPLAY7SEG_setD3(char d){
	display[2] = d;
	DISPLAY7SEG_setDisplay(display);
}
// Sets Digit 4
void DISPLAY7SEG_setD4(char d){
	display[3] = d;
	DISPLAY7SEG_setDisplay(display);
}
// Sets screen brightness using 0-255 as valid vals
void DISPLAY7SEG_setBright(uint8_t Brightness){
	DISPLAY7SEG_sendCmd(BRIGHT_CONT, Brightness);
}

//Dec con
void DISPLAY7SEG_setDecimal(uint8_t on){
	if(on == 1){
		dec_control[0] = 0x10;
	}
	if(on == 0){
		dec_control[0] = 0x00;
	}
	if(on <= -1 || on >= 1){
		// Go to hell
		DISPLAY7SEG_setErrorState(1);
	}
}
void DISPLAY7SEG_setDegree(uint8_t on){
	if(on == 1){
		dec_control[0] = 0x20;
	}
	if(on == 0){
		dec_control[0] = 0x00;
	}
	if(on <= -1 || on >= 1){
		// Go to hell
		DISPLAY7SEG_setErrorState(1);
	}
}

// Sends special commands, internal use only most likely and debug use
void DISPLAY7SEG_sendCmd(uint8_t cmdType, uint8_t cmd){
	mraa_uart_write(UART,cmdType);
	mraa_uart_write(UART,cmd);
}
// Refreshes the display using a clear, reset, then re-entering the digits via commands
void DISPLAY7SEG_Display(){
	mraa_uart_write(UART,display[0]);
	mraa_uart_write(UART,display[1]);
	mraa_uart_write(UART,display[2]);
	mraa_uart_write(UART,display[3]);
	mraa_uart_write(UART,DEC_CONTROL);
	mraa_uart_write(UART,(dec_control[0] | dec_control[1]));
}


// Output ERR to screen
void DISPLAY7SEG_setErrorState(uint8_t errorSet){
	err = errorSet;
	DISPLAY7SEG_error();
}
void DISPLAY7SEG_error(){
	if(err != 0){
		display[0] = 0x00;
		display[1] = 'E';
		display[2] = 'r';
		display[3] = 'r';
		dec_control[0] = 0x00;
		dec_control[1] = 0x00;
		DISPLAY7SEG_Display();
	}
	void close(){
		mraa_uart_stop(UART);
		mraa_uart_deinit();
	}
}
