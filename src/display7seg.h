#ifndef DISPLAY7SEG_H
#define DISPLAY7SEG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mraa.h"
#include "hardware.h"

#include <stdint.h>


/*----- DEFINES SECTION -----*/

/*-- CMD --*/
#define CLEAR 0x76
#define DEC_CONTROL 0x77
#define RESET 0x81
#define BRIGHT_CONT 0x7A

/***VARS***/
char display[4];
uint8_t dec_control[2];
uint8_t err;
mraa_uart_context UART;


/*-- PROTO --*/
// Issues the RESET command, code found in defines
void DISPLAY7SEG_reset(){}

// Issues the CLEAR command, code found in defines
void DISPLAY7SEG_clear(){}

// Initializes the 7-SEG with a selected BAUD, BAUD Support found in defines
void DISPLAY7SEG_init(uint8_t RX, uint8_t BAUD){}

// Initializes the 7-SEG with default BAUD of 9600, BAUD Support found in defines
void DISPLAY7SEG_init(uint8_t RX){}

// Sets Digit 1
void DISPLAY7SEG_setD1(char d){}

// Sets Digit 2
void DISPLAY7SEG_setD2(char d){}

// Sets Digit 3
void DISPLAY7SEG_setD3(char d){}

// Sets Digit 4
void DISPLAY7SEG_setD4(char d){}

// Sets screen brightness using 0-255 as valid vals
void DISPLAY7SEG_setBright(uint8_t Brightness){}

//Dec con
void DISPLAY7SEG_setDecimal(uint8_t on){}

void DISPLAY7SEG_setDegree(uint8_t on){}

// Sends special commands, internal use only most likely and debug use
void DISPLAY7SEG_sendCmd(uint8_t cmdType, uint8_t cmd){}

// Refreshes the display using a clear, reset, then re-entering the digits via commands
void DISPLAY7SEG_Display(){}

// Output ERR to screen
void DISPLAY7SEG_setErrorState(uint8_t errorSet){}

void DISPLAY7SEG_error(){}


#ifdef __cplusplus
}
#endif

#endif  /* DISPLAY7SEG_H */
